package task1

import (
	"reflect"
	"testing"
)

func TestCustomSort(t *testing.T) {
	tests := []struct {
		input    []string
		expected []string
	}{
		// Test case 1: Mixed words with varying 'a' count and length
		{
			input:    []string{"aaaasd", "a", "aab", "aaabcd", "ef", "cssssssd", "fdz", "kf", "zc", "lklklklklklklklkl", "l"},
			expected: []string{"aaaasd", "aaabcd", "aab", "a", "lklklklklklklklkl", "cssssssd", "fdz", "ef", "kf", "zc", "l"},
		},
		// Test case 2: Words with the same 'a' count but different lengths
		{
			input:    []string{"abc", "abcd", "ab", "a"},
			expected: []string{"abcd", "abc", "ab", "a"},
		},
		// Test case 3: Words with no 'a's
		{
			input:    []string{"xyz", "efg", "bcd"},
			expected: []string{"xyz", "efg", "bcd"},
		},
		// Test case 4: Words with only 'a's
		{
			input:    []string{"a", "aa", "aaa", "aaaa"},
			expected: []string{"aaaa", "aaa", "aa", "a"},
		},
	}

	for _, test := range tests {
		output := CustomSort(test.input)
		if !reflect.DeepEqual(output, test.expected) {
			t.Errorf("For input %v, expected %v but got %v", test.input, test.expected, output)
		}
	}
}
