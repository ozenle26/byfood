package task1

import (
	"sort"
)

// countA counts the number of occurrences of character 'a' in a given word.
func countA(word string) int {
	count := 0
	for _, char := range word {
		if char == 'a' {
			count++
		}
	}
	return count
}

// customSort sorts a slice of words based on the specified criteria.
func CustomSort(words []string) []string {
	// Sort the words using a custom comparison function.
	sort.Slice(words, func(i, j int) bool {
		countA_i := countA(words[i])
		countA_j := countA(words[j])

		// If the count of 'a's is the same, sort by length in descending order.
		if countA_i == countA_j {
			return len(words[i]) > len(words[j])
		}

		// Otherwise, sort by the count of 'a's in descending order.
		return countA_i > countA_j
	})

	return words
}
