# byfood



## Getting started

You can run unit tests for task1, task2 and task3 with this command:

```sh
cd byfood
go test ./...
```
For task4 I don't recommend to run unit tests like that because database unit tests,
need to run docker image in local environment.

You can find the task4 readme file [here](task4/README.md).

![Alt Text](static/mainview.gif)