package task2

import (
	"fmt"
	"math"
)

// printPerfectSquares prints perfect squares less than or equal to n.
func printPerfectSquares(n int) {
	// Start the recursive printing from 2 (smallest perfect square)
	printPerfectSquaresRecursive(n, 2)
}

// printPerfectSquaresRecursive is a helper function for recursively printing perfect squares.
func printPerfectSquaresRecursive(n, current int) {
	// Base case: If the current number is 2, print it (smallest perfect square)
	if current == 2 && n > 1 {
		fmt.Println(current)
	}

	// If the current number is within the range [2, n]
	if current <= n {
		// Calculate the integer square root of the current number
		sqrt := int64(int(math.Sqrt(float64(current))))

		// Check if the square of the integer square root is equal to the current number
		if sqrt*sqrt == int64(current) {
			fmt.Println(current) // Print the current perfect square
		}

		// Recur with the next number
		printPerfectSquaresRecursive(n, current+1)
	}
}
