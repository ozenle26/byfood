package task2

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"testing"
)

// captureOutput captures the standard output of a function.
func captureOutput(f func()) string {
	// Create a pipe to capture the output
	reader, writer, _ := os.Pipe()
	oldStdout := os.Stdout // Store the original os.Stdout
	os.Stdout = writer     // Redirect os.Stdout to the writer
	defer func() {
		os.Stdout = oldStdout // Restore the original os.Stdout
	}()

	// Call the function that uses fmt.Println
	f()

	// Close the writer and read the captured output
	writer.Close()
	capturedOutput := make(chan string)
	go func() {
		var buf bytes.Buffer
		io.Copy(&buf, reader)
		capturedOutput <- buf.String()
	}()
	return <-capturedOutput
}

func TestPrintPerfectSquares(t *testing.T) {
	// Define test cases with expected outputs
	testCases := []struct {
		input    int
		expected string
	}{
		{-1, ""},
		{0, ""},
		{2, "2\n"},
		{9, "2\n4\n9\n"},
		{11, "2\n4\n9\n"},
		{16, "2\n4\n9\n16\n"},
		{25, "2\n4\n9\n16\n25\n"},
		// Add more test cases here if needed
	}

	// Iterate through the test cases
	for _, tc := range testCases {
		t.Run(fmt.Sprintf("Input_%d", tc.input), func(t *testing.T) {
			// Capture the output of the printPerfectSquares function
			got := captureOutput(func() {
				printPerfectSquares(tc.input)
			})

			// Compare the captured output with the expected output
			if got != tc.expected {
				t.Errorf("Expected:\n%s\nGot:\n%s", tc.expected, got)
			}
		})
	}
}
