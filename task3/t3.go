package task3

// findMostRepeated takes an array of strings as input and returns the most repeated element.
func findMostRepeated(data []string) *string {
	counts := make(map[string]int)
	maxCount := 0
	mostRepeated := ""

	// Count the occurrences of each element in the input array.
	for _, item := range data {
		counts[item]++
		if counts[item] > maxCount {
			maxCount = counts[item]
			mostRepeated = item
		}
	}

	if maxCount == 0 {
		return nil
	}
	return &mostRepeated
}
