package task3

import (
	"testing"
)

// Helper function to create a pointer to a string with a specific value
func stringPointer(s string) *string {
	return &s
}

// TestFindMostRepeated is a test function to check the findMostRepeated function with various test cases.
func TestFindMostRepeated(t *testing.T) {
	testCases := []struct {
		input    []string
		expected *string
	}{
		{[]string{}, nil},
		{[]string{"", "a", ""}, stringPointer("")},
		{[]string{"apple", "pie", "apple", "red", "red", "red"}, stringPointer("red")}, // Test case with strings
		{[]string{"a", "b", "c", "b", "c", "c"}, stringPointer("c")},
		{[]string{"apple", "apple", "apple", "apple"}, stringPointer("apple")},
		{[]string{"apple", "banana", "orange", "grape"}, stringPointer("apple")},
		{[]string{"123", "456", "789", "123", "123", "789", "789"}, stringPointer("123")}, // Test case with number strings
	}

	// Iterate through test cases and compare the results.
	for _, testCase := range testCases {
		result := findMostRepeated(testCase.input)
		// Compare the dereferenced values of expected and actual results
		if result == nil && testCase.expected != nil {
			t.Errorf("For input %v, expected: %v, but got: %v", testCase.input, *testCase.expected, result)
		} else if result != nil && *result != *testCase.expected {
			t.Errorf("For input %v, expected: %v, but got: %v", testCase.input, *testCase.expected, *result)
		}
	}
}
